# Runner AWS Deploy (English)

![Infra as Code](./media/images/infrastructure-as-code.png)

> :dart: **Purpose**:
> The purpose of building this Docker image is to group the set of tools for provisioning infrastructure as code on AWS - (Amazon Web Services).

[English](#runner-aws-deploy-english) | [Português](#runner-aws-deploy-português) | [Español](#runner-aws-deploy-español)

## SUMMARY
1. [Involved Technologies](#involved-technologies)
2. [Version Checklist](#version-checklist)
3. [Usage](#usage)
    1. [AWS](#aws)
    2. [PYENV](#pyenv)
    3. [RBENV](#rbenv)
    4. [TFENV](#tfenv)
    5. [S3FS](#s3fs)
4. [FAQ - Contacts - Troubleshooting](#faq---contacts---troubleshooting)

### INVOLVED TECHNOLOGIES

## Version Checklist

| **SOFTWARE**   | **VERSION**       | **DESCRIPTION**                                           |
|----------------|-------------------|-----------------------------------------------------------|
| **Alpine**     | 3.12.1            | NAME="Alpine Linux" ID=alpine PRETTY_NAME="Alpine Linux v3.12" - HOME_URL="https://alpinelinux.org/" - BUG_REPORT_URL="https://bugs.alpinelinux.org/" |
| **awk**        | 5.1.0             | Copyright (C) 1989, 1991-2020 Free Software Foundation. This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/. |
| **aws**        | 2.15.60           | Python/3.8.10 Linux/5.15.154+ source-sandbox/x86_64.alpine.3 - https://aws.amazon.com/cli/ |
| **bash**       | 5.0.17(1)-release | x86_64-alpine-linux-musl Copyright (C) 2019 Free Software Foundation, Inc. License GPLv3+: GNU GPL version 3 or later http://gnu.org/licenses/gpl.html This is free software; you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law. - https://www.gnu.org/software/bash/ |
| **column**     | 2.37.4            | from util-linux - https://github.com/util-linux/util-linux |
| **pip**        | 23.0.1            | Package installer for Python - https://pip.pypa.io/        |
| **setuptools** | 65.5.0            | Package installer for Python - https://setuptools.pypa.io/ |
| **pyenv**      | 2.4.1             | Python version manager - https://github.com/pyenv/pyenv    |
| **python**     | 3.10.11           | Python interpreter - https://www.python.org/               |
| **rbenv**      | 1.2.0-91-gc3ba994 | Ruby version manager - https://github.com/rbenv/rbenv      |
| **ruby**       | 3.3.1             | (2024-04-23 revision c56cd86388) [x86_64-linux-musl] - https://www.ruby-lang.org/ |
| **s3fs**       | V1.94             | Amazon Simple Storage Service File System V1.94 (commit:a4f694c) with OpenSSL Copyright (C) 2010 Randy Rizun <rrizun@gmail.com> License GPL2: GNU GPL version 2 https://gnu.org/licenses/gpl.html This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law. - https://github.com/s3fs-fuse/s3fs-fuse |
| **ssh**        | 8.3p1             | OpenSSH_8.3p1, OpenSSL 1.1.1o 3 May 2022 - https://www.openssh.com/ |
| **terraform**  | 1.4.6             | on linux_amd64. Your version of Terraform is out of date! The latest version is 1.8.4. You can update by downloading from https://www.terraform.io/downloads.html - https://www.terraform.io/ |
| **tfenv**      | 3.0.0             | Terraform version manager - https://github.com/tfutils/tfenv |
| **tree**       | 1.8.0             | (c) 1996 - 2018 by Steve Baker, Thomas Moore, Francesc Rocher, Florian Sesser, Kyosuke Tokoro - https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard |
| **zip**        | 3.0               | Copyright (c) 1990-2008 Info-ZIP - Type 'zip "-L"' for software license. Currently maintained by E. Gordon. Please send bug reports to the authors using the web page at http://www.info-zip.org/ see README for details. Latest sources and executables are at ftp://ftp.info-zip.org/pub/infozip, as of above date; see http://www.info-zip.org/ for other sites. Compiled with gcc 9.3.0 for Unix (Linux ELF) on Jan 19 2020. Zip special compilation options: USE_EF_UT_TIME (store Universal Time) SYMLINK_SUPPORT (symbolic links supported) LARGE_FILE_SUPPORT (can read and write large files on file system) ZIP64_SUPPORT (use Zip64 to store large files in archives) UNICODE_SUPPORT (store and read UTF-8 Unicode paths) STORE_UNIX_UIDs_GIDs (store UID/GID sizes/values using new extra field) UIDGID_NOT_16BIT (old Unix 16-bit UID/GID extra field not used) [encryption, version 2.91 of 05 Jan 2007] (modified for Zip 3) Encryption notice: The encryption code of this program is not copyrighted and is put in the public domain. It was originally written in Europe and, to the best of our knowledge, can be freely distributed in both source and object forms from any country, including the USA under License Exception TSU of the U.S. Export Administration Regulations (section 740.13(e)) of 6 June 2002. Zip environment options: ZIP: [none] ZIPOPT: [none] |

## USAGE

A service user is required, does not need access to the AWS console, and the permission considerations for which services this user will have access to should already be defined in the **IAM** (Identity and Access Management) policy.

#### AWS

To use this Docker image in the project, the AWS keys must be declared in the variables of the GitLab-CI project group, with the names:
- **AWS_ACCESS_KEY_ID**
- **AWS_SECRET_ACCESS_KEY**
- **AWS_DEFAULT_REGION**

GitLab-CI already has a regular expression for simple format validation and secure storage.

When starting the container, the **aws-cli** and **s3fs** configuration process will start without manual intervention.

#### PYENV

If you need to use another version of the Python interpreter, I recommend creating a variable with the following name: **PYTHON_VERSION** and it can be used as follows in the YAML pipeline code:

```yaml
before_script:
  - time pyenv install ${PYTHON_VERSION} && pyenv global ${PYTHON_VERSION} && python --version 
```

#### RBENV

If you need to use another Ruby gem, I recommend creating a variable with the following name: **RUBY_VERSION** and it can be used as follows in the YAML pipeline code:

```yaml
before_script:
  - time rbenv install $RUBY_VERSION && rbenv global $RUBY_VERSION && ruby --version 
```

#### TFENV

tfenv is also a versioning utility, like rbenv, but this one is for Terraform versioning. I also recommend creating an external variable with the name **TERRAFORM_VERSION**, and used as follows:

```yaml
before_script:
  - time tfenv install ${TERRAFORM_VERSION} && tfenv use ${TERRAFORM_VERSION} && terraform --version 
```

#### S3FS

The s3fs utility is used to mount a bucket directly in the pipeline container. Since installation, build, and configuration have already been done, now it is just to use. To mount, a mount directory is required, recommended to be below **/mnt**. The suggested mount point is **/mnt/AWSS3**, it can be changed as needed, but before running the mount command, the point must already exist, i.e., the directory must already exist. I recommend creating the external variable **AWS_BUCKET**, containing only the bucket name. The client will resolve the protocol. If multiple mount points and multiple buckets are needed, organize it meaningfully.

```yaml
before_script:
  - s3fs ${AWS_BUCKET} /mnt/AWSS3 -o passwd_file=${HOME}/.passwd-s3fs -o nonempty 
  - df -h
```

The **df** command is only for inspecting the mount point and the output will be something like this, pay attention to the last entry:

```sh
$ df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                  21.9G      5.3G     15.6G  25% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                     1.8G         0      1.8G   0% /sys/fs/cgroup
/dev/sda9                21.9G      5.3G     15.6G  25% /builds
/dev/sda9                21.9G      5.3G     15.6G  25% /certs/client
/dev/sda9                21.9G      5.3G     15.6G  25% /etc/resolv.conf
/dev/sda9                21.9G      5.3G     15.6G  25% /etc/hostname
/dev/sda9                21.9G      5.3G     15.6G  25% /etc/hosts
shm                      64.0M         0     64.0M   0% /dev/shm
/dev/sda9                21.9G      5.3G     15.6G  25% /sys/devices/virtual/dmi/id
s3fs                    256.0T         0    256.0T   0% /mnt/AWSS3
```

## FAQ - Contacts - Troubleshooting
- **William Alves** - info@ramp-up.tech

# Runner AWS Deploy (Português)

![Infra as Code](./media/images/infrastructure-as-code.png)

> :dart: **Propósito**:
> O propósito da construção desta imagem Docker é agrupar o conjunto de ferramentas para a provisão de infraestrutura como código na AWS - (Amazon Web Services).

[English](#runner-aws-deploy-english) | [Português](#runner-aws-deploy-português) | [Español](#runner-aws-deploy-español)

## SUMÁRIO
1. [Tecnologias Envolvidas](#tecnologias-envolvidas)
2. [Check List de Versões](#check-list-de-versões)
3. [Utilização](#utilização)
    1. [AWS](#aws)
    2. [PYENV](#pyenv)
    3. [RBENV](#rbenv)
    4. [TFENV](#tfenv)
    5. [S3FS](#s3fs)
4. [FAQ - Contatos - Troubleshooting](#faq---contatos---troubleshooting)

### TECNOLOGIAS ENVOLVIDAS

## Check List de Versões

| **SOFTWARE**   | **VERSÃO**        | **DESCRIÇÃO**                                             |
|----------------|-------------------|-----------------------------------------------------------|
| **Alpine**     | 3.12.1            | NAME="Alpine Linux" ID=alpine PRETTY_NAME="Alpine Linux v3.12" - HOME_URL="https://alpinelinux.org/" - BUG_REPORT_URL="https://bugs.alpinelinux.org/" |
| **awk**        | 5.1.0             | Copyright (C) 1989, 1991-2020 Free Software Foundation. This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/. |
| **aws**        | 2.15.60           | Python/3.8.10 Linux/5.15.154+ source-sandbox/x86_64.alpine.3 - https://aws.amazon.com/cli/ |
| **bash**       | 5.0.17(1)-release | x86_64-alpine-linux-musl Copyright (C) 2019 Free Software Foundation, Inc. License GPLv3+: GNU GPL version 3 or later http://gnu.org/licenses/gpl.html This is free software; you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law. - https://www.gnu.org/software/bash/ |
| **column**     | 2.37.4            | from util-linux - https://github.com/util-linux/util-linux |
| **pip**        | 23.0.1            | Package installer for Python - https://pip.pypa.io/        |
| **setuptools** | 65.5.0            | Package installer for Python - https://setuptools.pypa.io/ |
| **pyenv**      | 2.4.1             | Python version manager - https://github.com/pyenv/pyenv    |
| **python**     | 3.10.11           | Python interpreter - https://www.python.org/               |
| **rbenv**      | 1.2.0-91-gc3ba994 | Ruby version manager - https://github.com/rbenv/rbenv      |
| **ruby**       | 3.3.1             | (2024-04-23 revision c56cd86388) [x86_64-linux-musl] - https://www.ruby-lang.org/ |
| **s3fs**       | V1.94             | Amazon Simple Storage Service File System V1.94 (commit:a4f694c) with OpenSSL Copyright (C) 2010 Randy Rizun <rrizun@gmail.com> License GPL2: GNU GPL version 2 https://gnu.org/licenses/gpl.html This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law. - https://github.com/s3fs-fuse/s3fs-fuse |
| **ssh**        | 8.3p1             | OpenSSH_8.3p1, OpenSSL 1.1.1o 3 May 2022 - https://www.openssh.com/ |
| **terraform**  | 1.4.6             | on linux_amd64. Your version of Terraform is out of date! The latest version is 1.8.4. You can update by downloading from https://www.terraform.io/downloads.html - https://www.terraform.io/ |
| **tfenv**      | 3.0.0             | Terraform version manager - https://github.com/tfutils/tfenv |
| **tree**       | 1.8.0             | (c) 1996 - 2018 by Steve Baker, Thomas Moore, Francesc Rocher, Florian Sesser, Kyosuke Tokoro - https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard |
| **zip**        | 3.0               | Copyright (c) 1990-2008 Info-ZIP - Type 'zip "-L"' for software license. Currently maintained by E. Gordon. Please send bug reports to the authors using the web page at http://www.info-zip.org/ see README for details. Latest sources and executables are at ftp://ftp.info-zip.org/pub/infozip, as of above date; see http://www.info-zip.org/ for other sites. Compiled with gcc 9.3.0 for Unix (Linux ELF) on Jan 19 2020. Zip special compilation options: USE_EF_UT_TIME (store Universal Time) SYMLINK_SUPPORT (symbolic links supported) LARGE_FILE_SUPPORT (can read and write large files on file system) ZIP64_SUPPORT (use Zip64 to store large files in archives) UNICODE_SUPPORT (store and read UTF-8 Unicode paths) STORE_UNIX_UIDs_GIDs (store UID/GID sizes/values using new extra field) UIDGID_NOT_16BIT (old Unix 16-bit UID/GID extra field not used) [encryption, version 2.91 of 05 Jan 2007] (modified for Zip 3) Encryption notice: The encryption code of this program is not copyrighted and is put in the public domain. It was originally written in Europe and, to the best of our knowledge, can be freely distributed in both source and object forms from any country, including the USA under License Exception TSU of the U.S. Export Administration Regulations (section 740.13(e)) of 6 June 2002. Zip environment options: ZIP: [none] ZIPOPT: [none] |



## UTILIZAÇÃO

É necessário um usuário de serviço, não precisa de acesso ao console AWS, e as considerações de permissão para quais serviços esse usuário terá acesso devem já ser definidas na política do **IAM** (Identity and Access Management).

#### AWS

Para usar esta imagem Docker no projeto, as chaves da AWS devem ser declaradas nas variáveis do grupo do projeto GitLab-CI, com os nomes:
- **AWS_ACCESS_KEY_ID**
- **AWS_SECRET_ACCESS_KEY**
- **AWS_DEFAULT_REGION**

O GitLab-CI já possui uma expressão regular para validação de formato simples e armazenamento seguro.

Ao iniciar o contêiner, o processo de configuração do **aws-cli** e do **s3fs** será iniciado sem intervenção manual.

#### PYENV

Se precisar usar outra versão do interpretador Python, recomendo a criação de uma variável com o seguinte nome: **PYTHON_VERSION** e pode ser usada da seguinte forma no código YAML do pipeline:

```yaml
before_script:
  - time pyenv install ${PYTHON_VERSION} && pyenv global ${PYTHON_VERSION} && python --version 
```

#### RBENV

Se precisar usar outra gem Ruby, recomendo a criação de uma variável com o seguinte nome: **RUBY_VERSION** e pode ser usada da seguinte forma no código YAML do pipeline:

```yaml
before_script:
  - time rbenv install $RUBY_VERSION && rbenv global $RUBY_VERSION && ruby --version 
```

#### TFENV

tfenv também é uma utilidade de versionamento, como o rbenv, mas este é para versionamento de Terraform. Recomendo também a criação de uma variável externa com o nome **TERRAFORM_VERSION**, e utilizada da seguinte forma:

```yaml
before_script:
  - time tfenv install ${TERRAFORM_VERSION} && tfenv use ${TERRAFORM_VERSION} && terraform --version 
```

#### S3FS

A utilidade s3fs serve para montar um bucket diretamente no contêiner do pipeline. Desde a instalação, construção e configuração já se realizaram, agora é só usar. Para montar é necessário um diretório de montagem, recomendado que seja abaixo de **/mnt**. O ponto de montagem sugerido é **/mnt/AWSS3**, pode ser alterado conforme a necessidade, mas antes de executar o comando de montagem o ponto já deve existir, ou seja, o diretório já deve existir. Recomendo a criação da variável externa **AWS_BUCKET**, contendo apenas o nome do bucket. O cliente resolverá o protocolo. Se forem necessários múltiplos pontos de montagem e múltiplos buckets, organize de forma significativa.

```yaml
before_script:
  - s3fs ${AWS_BUCKET} /mnt/AWSS3 -o passwd_file=${HOME}/.passwd-s3fs -o nonempty 
  - df -h
```

O comando **df** serve apenas para inspecionar o ponto de montagem e a saída será algo assim, preste atenção no último registro:

```sh
$ df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                  21.9G      5.3G     15.6G  25% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                     1.8G         0      1.8G   0% /sys/fs/cgroup
/dev/sda9                21.9G      5.3G     15.6G  25% /builds
/dev/sda9                21.9G      5.3G     15.6G  25% /certs/client
/dev/sda9                21.9G      5.3G     15.6G  25% /etc/resolv.conf
/dev/sda9                21.9G      5.3G     15.6G  25% /etc/hostname
/dev/sda9                21.9G      5.3G     15.6G  25% /etc/hosts
shm                      64.0M         0     64.0M   0% /dev/shm
/dev/sda9                21.9G      5.3G     15.6G  25% /sys/devices/virtual/dmi/id
s3fs                    256.0T         0    256.0T   0% /mnt/AWSS3
```

## FAQ - Contatos - Troubleshooting
- **William Alves** - info@ramp-up.tech

# Runner AWS Deploy (Español)

![Infra as Code](./media/images/infrastructure-as-code.png)

> :dart: **Propósito**:
> El propósito de construir esta imagen de Docker es agrupar el conjunto de herramientas para la provisión de infraestructura como código en AWS - (Amazon Web Services).

[English](#runner-aws-deploy-english) | [Português](#runner-aws-deploy-português) | [Español](#runner-aws-deploy-español)

## SUMARIO
1. [Tecnologías Involucradas](#tecnologías-involucradas)
2. [Lista de Verificación de Versiones](#lista-de-verificación-de-versiones)
3. [Uso](#uso)
    1. [AWS](#aws)
    2. [PYENV](#pyenv)
    3. [RBENV](#rbenv)
    4. [TFENV](#tfenv)
    5. [S3FS](#s3fs)
4. [FAQ - Contactos - Troubleshooting](#faq---contactos---troubleshooting)

### TECNOLOGÍAS INVOLUCRADAS

## Lista de Verificación de Versiones

| **SOFTWARE**   | **VERSIÓN**       | **DESCRIPCIÓN**                                           |
|----------------|-------------------|-----------------------------------------------------------|
| **Alpine**     | 3.12.1            | NAME="Alpine Linux" ID=alpine PRETTY_NAME="Alpine Linux v3.12" - HOME_URL="https://alpinelinux.org/" - BUG_REPORT_URL="https://bugs.alpinelinux.org/" |
| **awk**        | 5.1.0             | Copyright (C) 1989, 1991-2020 Free Software Foundation. This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/. |
| **aws**        | 2.15.60           | Python/3.8.10 Linux/5.15.154+ source-sandbox/x86_64.alpine.3 - https://aws.amazon.com/cli/ |
| **bash**       | 5.0.17(1)-release | x86_64-alpine-linux-musl Copyright (C) 2019 Free Software Foundation, Inc. License GPLv3+: GNU GPL version 3 or later http://gnu.org/licenses/gpl.html This is free software; you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law. - https://www.gnu.org/software/bash/ |
| **column**     | 2.37.4            | from util-linux - https://github.com/util-linux/util-linux |
| **pip**        | 23.0.1            | Package installer for Python - https://pip.pypa.io/        |
| **setuptools** | 65.5.0            | Package installer for Python - https://setuptools.pypa.io/ |
| **pyenv**      | 2.4.1             | Python version manager - https://github.com/pyenv/pyenv    |
| **python**     | 3.10.11           | Python interpreter - https://www.python.org/               |
| **rbenv**      | 1.2.0-91-gc3ba994 | Ruby version manager - https://github.com/rbenv/rbenv      |
| **ruby**       | 3.3.1             | (2024-04-23 revision c56cd86388) [x86_64-linux-musl] - https://www.ruby-lang.org/ |
| **s3fs**       | V1.94             | Amazon Simple Storage Service File System V1.94 (commit:a4f694c) with OpenSSL Copyright (C) 2010 Randy Rizun <rrizun@gmail.com> License GPL2: GNU GPL version 2 https://gnu.org/licenses/gpl.html This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law. - https://github.com/s3fs-fuse/s3fs-fuse |
| **ssh**        | 8.3p1             | OpenSSH_8.3p1, OpenSSL 1.1.1o 3 May 2022 - https://www.openssh.com/ |
| **terraform**  | 1.4.6             | on linux_amd64. Your version of Terraform is out of date! The latest version is 1.8.4. You can update by downloading from https://www.terraform.io/downloads.html - https://www.terraform.io/ |
| **tfenv**      | 3.0.0             | Terraform version manager - https://github.com/tfutils/tfenv |
| **tree**       | 1.8.0             | (c) 1996 - 2018 by Steve Baker, Thomas Moore, Francesc Rocher, Florian Sesser, Kyosuke Tokoro - https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard |
| **zip**        | 3.0               | Copyright (c) 1990-2008 Info-ZIP - Type 'zip "-L"' for software license. Currently maintained by E. Gordon. Please send bug reports to the authors using the web page at http://www.info-zip.org/ see README for details. Latest sources and executables are at ftp://ftp.info-zip.org/pub/infozip, as of above date; see http://www.info-zip.org/ for other sites. Compiled with gcc 9.3.0 for Unix (Linux ELF) on Jan 19 2020. Zip special compilation options: USE_EF_UT_TIME (store Universal Time) SYMLINK_SUPPORT (symbolic links supported) LARGE_FILE_SUPPORT (can read and write large files on file system) ZIP64_SUPPORT (use Zip64 to store large files in archives) UNICODE_SUPPORT (store and read UTF-8 Unicode paths) STORE_UNIX_UIDs_GIDs (store UID/GID sizes/values using new extra field) UIDGID_NOT_16BIT (old Unix 16-bit UID/GID extra field not used) [encryption, version 2.91 of 05 Jan 2007] (modified for Zip 3) Encryption notice: The encryption code of this program is not copyrighted and is put in the public domain. It was originally written in Europe and, to the best of our knowledge, can be freely distributed in both source and object forms from any country, including the USA under License Exception TSU of the U.S. Export Administration Regulations (section 740.13(e)) of 6 June 2002. Zip environment options: ZIP: [none] ZIPOPT: [none] |



## USO

Se requiere un usuario de servicio, no necesita acceso a la consola de AWS, y las consideraciones de permiso para qué servicios tendrá acceso este usuario ya deben definirse en la política de **IAM** (Identity and Access Management).

#### AWS

Para usar esta imagen de Docker en el proyecto

, las claves de AWS deben ser declaradas en las variables del grupo de proyectos GitLab-CI, con los nombres:
- **AWS_ACCESS_KEY_ID**
- **AWS_SECRET_ACCESS_KEY**
- **AWS_DEFAULT_REGION**

GitLab-CI ya tiene una expresión regular para la validación de formato simple y almacenamiento seguro.

Al iniciar el contenedor, el proceso de configuración de **aws-cli** y **s3fs** comenzará sin intervención manual.

#### PYENV

Si necesita usar otra versión del intérprete de Python, recomiendo crear una variable con el siguiente nombre: **PYTHON_VERSION** y puede usarse de la siguiente manera en el código YAML del pipeline:

```yaml
before_script:
  - time pyenv install ${PYTHON_VERSION} && pyenv global ${PYTHON_VERSION} && python --version 
```

#### RBENV

Si necesita usar otra gema de Ruby, recomiendo crear una variable con el siguiente nombre: **RUBY_VERSION** y puede usarse de la siguiente manera en el código YAML del pipeline:

```yaml
before_script:
  - time rbenv install $RUBY_VERSION && rbenv global $RUBY_VERSION && ruby --version 
```

#### TFENV

tfenv también es una utilidad de versionado, como rbenv, pero esta es para versionado de Terraform. También recomiendo crear una variable externa con el nombre **TERRAFORM_VERSION**, y se utiliza de la siguiente manera:

```yaml
before_script:
  - time tfenv install ${TERRAFORM_VERSION} && tfenv use ${TERRAFORM_VERSION} && terraform --version 
```

#### S3FS

La utilidad s3fs se utiliza para montar un bucket directamente en el contenedor del pipeline. Desde la instalación, construcción y configuración ya se han realizado, ahora solo es usar. Para montar se requiere un directorio de montaje, recomendado que esté debajo de **/mnt**. El punto de montaje sugerido es **/mnt/AWSS3**, puede cambiarse según sea necesario, pero antes de ejecutar el comando de montaje el punto ya debe existir, es decir, el directorio ya debe existir. Recomiendo crear la variable externa **AWS_BUCKET**, que contiene solo el nombre del bucket. El cliente resolverá el protocolo. Si se necesitan múltiples puntos de montaje y múltiples buckets, organícelos de manera significativa.

```yaml
before_script:
  - s3fs ${AWS_BUCKET} /mnt/AWSS3 -o passwd_file=${HOME}/.passwd-s3fs -o nonempty 
  - df -h
```

El comando **df** es solo para inspeccionar el punto de montaje y la salida será algo así, preste atención a la última entrada:

```sh
$ df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                  21.9G      5.3G     15.6G  25% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                     1.8G         0      1.8G   0% /sys/fs/cgroup
/dev/sda9                21.9G      5.3G     15.6G  25% /builds
/dev/sda9                21.9G      5.3G     15.6G  25% /certs/client
/dev/sda9                21.9G      5.3G     15.6G  25% /etc/resolv.conf
/dev/sda9                21.9G      5.3G     15.6G  25% /etc/hostname
/dev/sda9                21.9G      5.3G     15.6G  25% /etc/hosts
shm                      64.0M         0     64.0M   0% /dev/shm
/dev/sda9                21.9G      5.3G     15.6G  25% /sys/devices/virtual/dmi/id
s3fs                    256.0T         0    256.0T   0% /mnt/AWSS3
```

## FAQ - Contactos - Troubleshooting
- **William Alves** - info@ramp-up.tech