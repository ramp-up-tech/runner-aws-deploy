#!/bin/bash

USER_ID=${LOCAL_USER_ID:-9001}

echo "Starting with UID : $USER_ID"
addgroup -g $USER_ID user && adduser -D -G user -s /bin/false -u $USER_ID user
export HOME=/home/user

# Set environment
echo 'export PATH="${RBENV_ROOT}/bin:${RBENV_ROOT}/shims:$PATH"' >> /etc/profile.d/rbenv.sh
echo 'eval "$(rbenv init -)"'      >> /etc/profile.d/rbenv.sh
echo 'eval "$(rbenv init -)"'      >> /etc/profile.d/rbenv.sh
echo 'eval "$(pyenv init --path)"' >> /etc/profile.d/pyenv.sh

# Load environment
source /etc/profile.d/pyenv.sh && source /etc/profile.d/rbenv.sh

mkdir -p ${HOME}/git
mkdir -p /home/user/.cache/pip

# Create key - s3fs
echo ${AWS_ACCESS_KEY_ID}:${AWS_SECRET_ACCESS_KEY} > ${HOME}/.passwd-s3fs
chmod 600 ${HOME}/.passwd-s3fs

# Create directory to mount point
mkdir -p /mnt/AWSS3

exec /bin/bash
