# Use the docker:dind image as the base
FROM docker:stable-dind

# Define the argument for the Python version
ARG RUBY_VERSION=3.3.1
ARG PYTHON_VERSION=3.10.11

# Set environment variables
ENV RUBY_VERSION=${RUBY_VERSION}
ENV PYTHON_VERSION=${PYTHON_VERSION}
ENV CONFIGURE_OPTS="--disable-install-doc"

# Define a base directory for environment variables
ENV BASE_DIR=/usr/local

# Set environment variables for specific tools
ENV PYENV_ROOT=${BASE_DIR}/pyenv
ENV RBENV_ROOT=${BASE_DIR}/rbenv
ENV TFENV_ROOT=${BASE_DIR}/tfenv
ENV AWSENV_ROOT=${BASE_DIR}/awsenv

# Construct the PATH environment variable efficiently
ENV PATH="${RBENV_ROOT}/bin:${RBENV_ROOT}/shims:$PATH"
ENV PATH_DFT="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
ENV PATH="${TFENV_ROOT}/bin:${PYENV_ROOT}/bin:${AWSENV_ROOT}/lib/aws-cli/bin:${PATH_DFT}"

# Install essential dependencies
RUN echo "Installing essential dependencies" \
    && time apk add --no-cache \
        alpine-sdk \
        jq \
        xz \
        git \
        zip \
        bash \
        gawk \
        perl \
        tree \
        bzip2 \
        unzip \
        tzdata \
        xz-dev \
        openssh \
        py3-pip \
        python3 \
        tcl-dev \
        zlib-dev \
        yaml-dev \
        bzip2-dev \
        expat-dev \
        libffi-dev \
        libxslt-dev \
        sqlite-dev \
        util-linux \
        ncurses-dev \
        openssl-dev \
        python3-dev \
        readline-dev \
        linux-headers \
    && rm -rf /var/cache/apk/*

# Set up tfenv
RUN echo "Setting up tfenv" \
    && git clone --depth 1 https://github.com/tfutils/tfenv.git ${TFENV_ROOT}

# Set up pyenv
RUN echo "Setting up pyenv" \
    && curl -s https://pyenv.run | bash \
    && cd ${PYENV_ROOT}/plugins/python-build/../.. && git pull && cd - \
    && eval "$(pyenv init --path)"

# Install rbenv, ruby-build plugin, and set up the environment
RUN echo "Install rbenv, ruby-build plugin" \
    && mkdir -p ${RBENV_ROOT} \
    && git clone https://github.com/rbenv/rbenv.git ${RBENV_ROOT} \
    && mkdir -p ${RBENV_ROOT}/plugins \
    && git clone https://github.com/rbenv/ruby-build.git ${RBENV_ROOT}/plugins/ruby-build \
    && ${RBENV_ROOT}/plugins/ruby-build/install.sh

# Install the specified Python version
RUN echo "Installing Python ${PYTHON_VERSION}" \
    && time pyenv update \
    && time pyenv install ${PYTHON_VERSION} \
    && time pyenv global ${PYTHON_VERSION}
    
# Install the specified Ruby version
RUN echo "Installing Ruby ${RUBY_VERSION}" \
    && echo 'export PATH="${RBENV_ROOT}/bin:${RBENV_ROOT}/shims:$PATH"' >> /etc/profile.d/rbenv.sh \
    && echo 'eval "$(rbenv init -)"' >> /etc/profile.d/rbenv.sh \
    && /bin/bash -c "source /etc/profile.d/rbenv.sh && rbenv --version && rbenv install ${RUBY_VERSION} && rbenv global ${RUBY_VERSION}"

# Install the specified Terraform version
RUN echo "Installing Terraform latest:^1.4.6" \
    && time tfenv install latest:^1.4.6 \
    && time tfenv use latest:^1.4.6

# Install boto3
RUN echo "Installing boto3" \
    && pip install --upgrade pip \    
    && pip3 install boto3

# Instalar aws-cli versão 2
RUN echo "Installing aws-cli version 2" \
    && curl -o awscli.tar.gz https://awscli.amazonaws.com/awscli.tar.gz \
    && tar -xzf awscli.tar.gz \
    && rm -rf awscli.tar.gz \
    && export DIR_AWSCLI=$(ls -lrt | grep awscli | grep ^d | awk '{print $NF}') \
    && cd ${DIR_AWSCLI} && time ./configure --prefix=${AWSENV_ROOT} --with-download-deps \
    && time make && time make install

# Install s3fs dependencies and set up s3fs
RUN echo "Installing s3fs dependencies and setting up s3fs" \
    && apk add --no-cache \
        fuse \
        fuse-dev \
        libxml2-dev \
        openssl-dev \
        curl-dev \
        pkgconfig \
    && apk add --virtual build-dependencies --no-cache \
        build-base \
        automake \
        autoconf \
        libressl-dev \
    && git clone --depth 1 https://github.com/s3fs-fuse/s3fs-fuse.git \
    && cd s3fs-fuse \
    && ./autogen.sh \
    && ./configure --prefix=/usr \
    && make \
    && make install \
    && apk del build-dependencies \
    && rm -rf /s3fs-fuse /var/cache/apk/*

# Copy the entrypoint script
COPY docker-entrypoint.sh /docker-entrypoint.sh

# Set the entrypoint
ENTRYPOINT ["bash", "/docker-entrypoint.sh"]
